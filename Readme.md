# VB Alarm Clock
![VB Alarm Clock](./screenshot.jpg) 

## Description
This is a simple alarm clock.  It will allow you to set your computer to play an audio file (.mp3 or .wav) at a specific time.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
After installing the program, specify a time and audio file and press "Turn Alarm On".  When the specified time is reached, the program will play the audio file.  

"Test Alarm" will start playing the audio file immediately, and "Stop Alarm" with stop the playback of either the alarm or an alarm test.

## History
**Version 1.0.0:**
> - Initial Release
> - Released on 28 February 2001
>
> Download: [Version 1.0.0 Setup](/uploads/4a9fcd64e46ba06cda322be645f24079/VBAlarmClockSetup100.zip) | [Version 1.0.0 Source](/uploads/d2bd05ef50ea20b923e88f92a69cd65d/VBAlarmClockSource100.zip)
