VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{6BF52A50-394A-11D3-B153-00C04F79FAA6}#1.0#0"; "wmp.dll"
Begin VB.Form frmAlarm 
   BackColor       =   &H00FFFFFF&
   Caption         =   "Alarm Clock"
   ClientHeight    =   2790
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7530
   Icon            =   "frmAlarm.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmAlarm.frx":0442
   ScaleHeight     =   2790
   ScaleWidth      =   7530
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdAlarmOff 
      Caption         =   "Stop Alarm"
      Height          =   615
      Left            =   5040
      TabIndex        =   14
      Top             =   1440
      Width           =   975
   End
   Begin VB.CommandButton cmdSelectFIle 
      Caption         =   "Select File"
      Height          =   495
      Left            =   6120
      TabIndex        =   13
      Top             =   840
      Width           =   975
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   1200
      Top             =   2040
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      DialogTitle     =   "Select File"
      Filter          =   "MP3 Files (*.mp3)|*.mp3|Wave Files (*.wav)|*.wav"
      InitDir         =   "c:\"
   End
   Begin VB.OptionButton optPM 
      BackColor       =   &H00FFFFFF&
      Caption         =   "PM"
      Height          =   195
      Left            =   5760
      TabIndex        =   12
      Top             =   360
      Width           =   615
   End
   Begin VB.OptionButton optAM 
      BackColor       =   &H00FFFFFF&
      Caption         =   "AM"
      Height          =   195
      Left            =   5760
      TabIndex        =   11
      Top             =   120
      Value           =   -1  'True
      Width           =   615
   End
   Begin VB.ComboBox cmboMin 
      Height          =   315
      ItemData        =   "frmAlarm.frx":2A88
      Left            =   4800
      List            =   "frmAlarm.frx":2B40
      TabIndex        =   9
      Top             =   120
      Width           =   615
   End
   Begin VB.ComboBox cmboHour 
      Height          =   315
      ItemData        =   "frmAlarm.frx":2C34
      Left            =   4080
      List            =   "frmAlarm.frx":2C5C
      TabIndex        =   8
      Top             =   120
      Width           =   615
   End
   Begin VB.CommandButton cmdTest 
      Caption         =   "Test Alarm"
      Height          =   615
      Left            =   2760
      TabIndex        =   5
      Top             =   1440
      Width           =   975
   End
   Begin VB.TextBox txtPlaylist 
      Height          =   315
      Left            =   2640
      TabIndex        =   4
      Top             =   960
      Width           =   3375
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "Exit"
      Height          =   615
      Left            =   6240
      TabIndex        =   2
      Top             =   1440
      Width           =   735
   End
   Begin VB.CommandButton cmdAlarm 
      Caption         =   "Turn Alarm On"
      Height          =   615
      Left            =   3840
      TabIndex        =   0
      Top             =   1440
      Width           =   975
   End
   Begin WMPLibCtl.WindowsMediaPlayer MediaPlayer1 
      Height          =   615
      Left            =   4440
      TabIndex        =   15
      Top             =   2160
      Visible         =   0   'False
      Width           =   735
      URL             =   ""
      rate            =   1
      balance         =   0
      currentPosition =   0
      defaultFrame    =   ""
      playCount       =   1
      autoStart       =   -1  'True
      currentMarker   =   0
      invokeURLs      =   -1  'True
      baseURL         =   ""
      volume          =   50
      mute            =   0   'False
      uiMode          =   "full"
      stretchToFit    =   0   'False
      windowlessVideo =   0   'False
      enabled         =   -1  'True
      enableContextMenu=   -1  'True
      fullScreen      =   0   'False
      SAMIStyle       =   ""
      SAMILang        =   ""
      SAMIFilename    =   ""
      captioningID    =   ""
      enableErrorDialogs=   0   'False
      _cx             =   1296
      _cy             =   1085
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   4560
      TabIndex        =   10
      Top             =   120
      Width           =   90
   End
   Begin VB.Image imgTimer 
      Height          =   75
      Left            =   3360
      Picture         =   "frmAlarm.frx":2C90
      Top             =   2400
      Width           =   300
   End
   Begin VB.Label lblAlarmStatus 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "off"
      Height          =   195
      Left            =   3240
      TabIndex        =   7
      Top             =   2160
      Width           =   180
   End
   Begin VB.Label lblStatus 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Status:"
      Height          =   195
      Left            =   2640
      TabIndex        =   6
      Top             =   2160
      Width           =   495
   End
   Begin VB.Label lblPlaylist 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Audio File:"
      Height          =   195
      Left            =   2640
      TabIndex        =   3
      Top             =   720
      Width           =   735
   End
   Begin VB.Label lblTime 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Alarm Time:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   2760
      TabIndex        =   1
      Top             =   120
      Width           =   1245
   End
   Begin VB.Image imgClock 
      Height          =   2505
      Left            =   0
      Top             =   0
      Width           =   2490
   End
End
Attribute VB_Name = "frmAlarm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Eric Cavaliere
' Program Name:  Alarm Clock
' Project File Name:  Alarm Clock.vbp
' Source Code File Name:  frmAlarm.frm
' Started on 02/24/2001
' Last updated on 02/28/2001

Option Explicit

Private Sub cmdAlarm_Click()
  ' Turn on the alarm.
  Dim direction As String
  Dim hour As String, min As String, startTime As String
  
  hour = cmboHour.List(cmboHour.ListIndex)
  min = cmboMin.List(cmboMin.ListIndex)
  If optPM.Value = True Then  ' adjust if time is pm
    hour = Str(Val(hour) + 12)
    If cmboHour.List(cmboHour.ListIndex) = "12" Then  ' if noon...
      hour = 12
    End If
  End If
  If optAM.Value = True Then  ' If midnight...
    If cmboHour.List(cmboHour.ListIndex) = 12 Then
      hour = "00"
    End If
  End If
  
  startTime = hour + ":" + min + ":00"
  direction = "l"
  lblAlarmStatus.Caption = "On, " + startTime
  While Time$ <> startTime
    DoEvents
    If (direction = "l") Then
      If (imgTimer.Left <> 2400) Then
        imgTimer.Left = imgTimer.Left - 1
      Else
        direction = "r"
      End If
    End If
    If (direction = "r") Then
      If (imgTimer.Left <> 7000) Then
        imgTimer.Left = imgTimer.Left + 1
      Else
        direction = "l"
      End If
    End If
  Wend
  
  'Play File
  MediaPlayer1.URL = LTrim$(RTrim$(txtPlaylist.Text))
  lblAlarmStatus.Caption = "Off"
End Sub

Private Sub cmdAlarmOff_Click()
  ' Turn off the alarm.
  MediaPlayer1.Close
End Sub

Private Sub cmdExit_Click()
  ' Quit the program.
  frmAlarm.Hide
  Unload frmAlarm
  End
End Sub

Private Sub cmdSelectFIle_Click()
  ' Display an Open File Dialog to allow
  '     the user to select an audio file.
  CommonDialog1.ShowOpen
  txtPlaylist.Text = CommonDialog1.FileName
End Sub

Private Sub cmdTest_Click()
  ' Start playing the selected file.
  MediaPlayer1.URL = LTrim$(RTrim$(txtPlaylist.Text))
End Sub

Private Sub Form_Load()
  Dim tempHour As Integer, tempMin As Integer, tempMusicFileLoc As String
  Dim datFile As String
  
  'auto load settings
  datFile = App.Path + "\alarmclockprefs.dat"
  Open datFile For Binary As #1
  If LOF(1) = 0 Then
    Close #1
    Open datFile For Output As #1
    Write #1, 5, 30, "Select a File"
    Close #1
  End If
  Close #1
  Open datFile For Input As #1
  Input #1, tempHour, tempMin, tempMusicFileLoc
  Close #1
  cmboHour.ListIndex = tempHour
  cmboMin.ListIndex = tempMin
  txtPlaylist.Text = tempMusicFileLoc
End Sub

Private Sub Form_Unload(Cancel As Integer)
  'auto save settings
  Dim datFile As String
  datFile = App.Path + "\alarmclockprefs.dat"
  Open datFile For Output As #1
  Write #1, cmboHour.ListIndex, cmboMin.ListIndex, txtPlaylist.Text
  Close #1
End Sub
